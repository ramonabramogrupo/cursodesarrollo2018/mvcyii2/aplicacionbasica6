<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Formulario1 */
/* @var $form ActiveForm */
?>
<div class="formulario2">

    <?php $form = ActiveForm::begin([
        'id' => 'formulario2',
        'options' => ['class' => 'form-horizontal'],
    ]); ?>
    <?= $form->errorSummary($model); ?>
        <?= $form->field($model, 'nombre') ?>
        <?= $form
                ->field($model, 'peso')
                ->textInput([
                    "type"=>"number"
                 ]) ?>
        <?= $form
                ->field($model, 'altura')
                ->textInput([
                    "type"=>"number"
                ]) ?>
        <?= $form
                ->field($model, 'poblacion')
                ->dropDownList(
                    $model->getValoresPoblacion(),[
                        'prompt'=>"Seleccione una poblacion"
                    ]); ?>
        <?= $form->field($model, 'apellidos') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- formulario2 -->
