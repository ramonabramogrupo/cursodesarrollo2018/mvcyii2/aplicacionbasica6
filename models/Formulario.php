<?php
namespace app\models;

use Yii;
use yii\base\Model;

class Formulario extends Model
{
    public $nombre;
    public $edad;

    public function rules(){
        return [
           [['nombre','edad'],"required",
               "message"=>"El campo {attribute} no puede estar vacio"],
            ['edad','integer',"message"=>'El campo edad debe ser numerico'],
           [['nombre','edad'],"safe"],
        ];
    }
    
    public function attributeLabels(){
        return [
            'nombre' => 'Nombre del trabajador',
            'edad' => 'Edad del trabajador',
        ];
    }
    
}
