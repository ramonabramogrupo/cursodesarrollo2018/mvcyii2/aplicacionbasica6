

    <?= \yii\widgets\DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'apellidos',
            'altura',
            'peso',
            'poblacion',
            [
                'attribute'=>"nombreCompleto",
                'value'=>function($model){
                    return $model->getNombreCompleto();
                }
            ],
            [
                'attribute'=>"IMC",
                'value'=>function($model){
                    return $model->getIMC();
                }
            ],
                    
        ],
    ]) ?>

